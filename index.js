
const FIRST_NAME = "Ana-Maria";
const LAST_NAME = "Schpak";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value> Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER){
        return NaN;
    } 
    return parseInt(value,10);
    

}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

